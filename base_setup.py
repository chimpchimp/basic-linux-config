#!/usr/bin/python

import os
import argparse
import tempfile
import urllib
import urllib2
import subprocess
import sys
import tarfile
import glob

if os.geteuid() != 0:
    os.execvp("sudo", ["sudo"] + sys.argv)

parser = argparse.ArgumentParser()
parser.add_argument('--vmware', action='store_true', help='Install VMware tools. ISO should be mounted in the CD-Rom drive.')
parser.add_argument('--webmin', action='store_true', help='Download and install the latested version of Webmin.')

results = parser.parse_args()

working_dir = tempfile.mkdtemp()

def webmin_install():

        webmindl = urllib2.urlopen('http://www.webmin.com/download/deb/webmin-current.deb').geturl()
        webminfile = os.path.join(working_dir, 'webmin-current.deb')
        print(webminfile)
        urllib.urlretrieve(webmindl, webminfile)

        subprocess.call('apt-get -y -f install libio-pty-perl libnet-ssleay-perl libauthen-pam-perl apt-show-versions libapt-pkg-perl', shell=True)
        subprocess.call(('dpkg -i ' +  webminfile), shell=True)

def vmware_install():
	iso_path = '/mnt/cdrom'
	if not os.path.exists(iso_path):
		os.makedirs(iso_path)
	if not os.path.ismount(iso_path):
		subprocess.call('mount /dev/cdrom /mnt/cdrom', shell=True)
	vmarchive = glob.glob(iso_path + '/*.tar.gz')[0]
	with tarfile.open(vmarchive, 'r:gz') as gz:
		gz.extractall(working_dir)
	vmscript = os.path.join(working_dir, 'vmware-tools-distrib',  'vmware-install.pl')
	subprocess.call((vmscript + ' -d'), shell=True)


if results.vmware is True:
	vmware_install()

if results.webmin is True:
	webmin_install()
